package pl.tkaminski.tkaminskirestjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TkaminskiRestJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TkaminskiRestJpaApplication.class, args);
    }

}

