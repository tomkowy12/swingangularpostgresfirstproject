package pl.tkaminski.tkaminskirestjpa;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.tkaminski.tkaminskirestjpa.model.*;
import pl.tkaminski.tkaminskirestjpa.repo.*;

import javax.xml.bind.SchemaOutputResolver;
import java.util.Date;

@Component
public class Start {


    private DoctorRepo doctorRepo;
    private HumanRepo humanRepo;
    private PatientRepo patientRepo;
    private UserRepo userRepo;
    private VisitRepo visitRepo;


    public Start(DoctorRepo doctorRepo, HumanRepo humanRepo, PatientRepo patientRepo, UserRepo userRepo, VisitRepo visitRepo) {
        this.doctorRepo = doctorRepo;
        this.humanRepo = humanRepo;
        this.patientRepo = patientRepo;
        this.userRepo = userRepo;
        this.visitRepo = visitRepo;
    }

    @EventListener (ApplicationReadyEvent.class)
    public void runExample()
    {
        Patient patient = new Patient(9, new Date(), new Date());
        Doctor doctor = new Doctor("pediatra", 9);
        Human human = new Human(9, "Andrzej","Kowalski", new Date(),"andrzej@gmail.com" );

        Patient patient2 = new Patient(10, new Date(), new Date());
        Doctor doctor2 = new Doctor("otolaryngolog", 10);
        Human human2 = new Human(10, "Alan","Kowalski", new Date(),"alan@gmail.com" );

        User user = new User(9, "user1", "1234", true);
        Visit visit = new Visit(9,10, new Date());

        try
        {
//            doctorRepo.save(doctor);
//            doctorRepo.save(doctor2);
//            visitRepo.save(visit);
//            humanRepo.save(human);
            patientRepo.save(patient);
        }
        catch(org.springframework.dao.DataIntegrityViolationException a)
        {
            System.out.println("DB update error");
        }

    }
}
