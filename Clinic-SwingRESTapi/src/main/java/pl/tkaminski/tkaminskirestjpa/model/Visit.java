package pl.tkaminski.tkaminskirestjpa.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "id_doctor")
   //@Column(name = "id_doctor")
    private Integer id_doctor;

    @Column(name = "id_patient")
    private Integer id_patient;

    public Visit(Integer id_doctor, Integer id_patient, Date visit_date) {
        this.id_doctor = id_doctor;
        this.id_patient = id_patient;
        this.visit_date = visit_date;
    }

    @Column(name = "date")
    private Date visit_date;
    public Visit() {
    }


    public Integer getId_doctor() {
        return id_doctor;
    }

    public void setId_doctor(Integer id_doctor) {
        this.id_doctor = id_doctor;
    }

    public Integer getId_patient() {
        return id_patient;
    }

    public void setId_patient(Integer id_patient) {
        this.id_patient = id_patient;
    }


    public String toString()
    {
        return id_doctor + "";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
