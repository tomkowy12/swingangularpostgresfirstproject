package pl.tkaminski.tkaminskirestjpa.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="patients")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "created_at")
    private Date created_at;


    @Column(name = "date_of_birth")
    private Date date;

    public Patient(Integer id, Date created_at, Date date) {
        this.id = id;
        this.created_at = created_at;
        this.date = date;
    }

    public Patient(){

    }

    public Integer get_id() {
        return id;
    }

    public void set_id(Integer _id) {
        this.id = _id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
