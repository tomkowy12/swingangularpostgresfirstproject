package pl.tkaminski.tkaminskirestjpa.model;

import javax.persistence.*;

@Entity
@Table(name="doctors")
public class Doctor {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "specjalization")
    private String specjalization;

    public Doctor(String specjalization, Integer id) {
        this.id = id;
        this.specjalization = specjalization;
    }

    public String getSpecjalization() {
        return specjalization;
    }

    public void setSpecjalization(String specjalization) {
        this.specjalization = specjalization;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
