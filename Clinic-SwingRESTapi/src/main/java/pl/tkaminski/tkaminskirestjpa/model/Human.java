package pl.tkaminski.tkaminskirestjpa.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="people")
public class Human {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "created_at")
    private Date created_at;

    @Column(name = "email")
    private String email;

    public Human(Integer id, String name, String surname, Date created_at, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.created_at = created_at;
        this.email = email;
    }

    @Column(name = "gender")
    private String gender;

    public Human() {
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
