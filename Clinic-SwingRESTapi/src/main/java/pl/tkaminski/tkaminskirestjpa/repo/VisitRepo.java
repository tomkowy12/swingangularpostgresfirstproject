package pl.tkaminski.tkaminskirestjpa.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.tkaminski.tkaminskirestjpa.model.Visit;

@Repository
public interface VisitRepo extends CrudRepository<Visit, Integer> {
}
