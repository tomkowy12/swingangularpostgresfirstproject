package pl.tkaminski.tkaminskirestjpa.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.tkaminski.tkaminskirestjpa.model.User;

@Repository
public interface UserRepo extends CrudRepository<User, Integer> {
}
