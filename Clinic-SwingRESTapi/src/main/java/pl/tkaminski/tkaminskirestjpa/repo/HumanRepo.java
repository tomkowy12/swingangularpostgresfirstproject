package pl.tkaminski.tkaminskirestjpa.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.tkaminski.tkaminskirestjpa.model.Human;

@Repository
public interface HumanRepo extends CrudRepository<Human, Integer> {
}
