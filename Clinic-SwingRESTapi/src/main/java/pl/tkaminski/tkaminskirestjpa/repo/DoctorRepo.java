package pl.tkaminski.tkaminskirestjpa.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.tkaminski.tkaminskirestjpa.model.Doctor;

@Repository
public interface DoctorRepo extends CrudRepository<Doctor, Integer> {

}
