package pl.tkaminski.tkaminskirestjpa.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.tkaminski.tkaminskirestjpa.model.Patient;

@Repository
public interface PatientRepo extends CrudRepository<Patient, Integer> {

}
