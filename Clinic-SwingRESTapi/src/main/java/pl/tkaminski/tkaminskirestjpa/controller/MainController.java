package pl.tkaminski.tkaminskirestjpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.tkaminski.tkaminskirestjpa.model.Patient;
import pl.tkaminski.tkaminskirestjpa.model.Visit;
import pl.tkaminski.tkaminskirestjpa.repo.HumanRepo;
import pl.tkaminski.tkaminskirestjpa.repo.PatientRepo;
import pl.tkaminski.tkaminskirestjpa.repo.VisitRepo;

import java.util.List;
import java.util.Optional;

//@RequestMapping("/zapytanka")
@RestController
public class MainController {

    private PatientRepo patientRepo;
    private VisitRepo visitRepo;
    private List<VisitRepo> visitList;
    private List<HumanRepo> peopleRepo;

    @Autowired
    public MainController(PatientRepo repo)
    {
        this.patientRepo = repo;
    }

    @GetMapping("/getPatients")
    public Iterable<Patient> getPatients()
    {
        return patientRepo.findAll();
    }


/*///////////////////////////////////////////

    private VisitRepo repoV;

    @Autowired
    public MainController(VisitRepo repo)
    {
        this.repoV = repo;
    }

//////////////////////////////////////*/

    //@GetMapping("/getPatients")
    //public Iterable<Patient> getPatients( {return repo.findAll();}

    @GetMapping("/getExampleRequestHeader")
    @ResponseBody
    public String requestHeaderExample(@RequestHeader("number") Long number) {
        return "delivered by RequestHeader: " + number;
    }

    @GetMapping
    public Optional<Patient> getById(@RequestParam Integer index) {
        return patientRepo.findById(index);
    }






    //////////////////////////////////




}
